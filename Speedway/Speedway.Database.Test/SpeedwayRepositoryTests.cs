﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Speedway.Database.Contexts;
using Speedway.Database.Entites;
using Speedway.Database.Models;
using Speedway.Database.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using Xunit;
using Xunit.Abstractions;

namespace Speedway.Database.Test
{
    public class SpeedwayRepositoryTests
    {
        private ITestOutputHelper _output;
        public SpeedwayRepositoryTests(ITestOutputHelper output)
        {
            _output = output;
        }


        [Fact]
        public void GetRoster()
        {
            var connectionStringBuilder =
                    new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t1 = new Team()
                {
                    Id = 4,
                    Name = "Stal Gorzów"
                };
                Team t2 = new Team
                {
                    Id = 2,
                    Name = "GKM Grudziądz"
                };
                context.Teams.AddRange(new Team[] { t1, t2 });
                Racer r1 = new Racer()
                {
                    Id = 2,
                    Name1 = "Bartosz",
                    Surname = "Zmarzlik",
                    Nationality1 = "Poland",
                    Birthday = new DateTime(1995, 04, 12)
                };

                Racer r2 = new Racer()
                {
                    Id = 3,
                    Name1 = "Artiom",
                    Name2 = "Grigorjewicz",
                    Surname = "Łaguta",
                    Nationality1 = "Russia",
                    Nationality2 = "Poland",
                    Birthday = new DateTime(1995, 11, 13)
                };

                Racer r3 = new Racer()
                {
                    Id = 1,
                    Name1 = "Woźniak",
                    Surname = "Szymon",
                    Nationality1 = "Poland",
                    Birthday = new DateTime(1993, 05, 06)
                };

                context.Racers.AddRange(new Racer[] { r1, r2, r3 });

                context.Rosters.AddRange(new Roster[]{
                    new Roster()
                    {
                        Racer = r1,
                        Season = "2019",
                        Team = t1
                    },
                    new Roster()
                    {
                        Racer = r1,
                        Season = "2020",
                        Team = t1
                    },
                     new Roster()
                    {
                        Racer = r2,
                        Season = "2020",
                        Team = t2
                    },
                     new Roster()
                    {
                        Racer = r3,
                        Season = "2020",
                        Team = t1
                    }
                }
                );


                context.SaveChanges();

                FullRosterTeam roster = null;

                SpeedwayRepository repository = new SpeedwayRepository(context);

                roster = repository.GetRosterForTeam(t1, "2020");

                Assert.Equal(t1, roster.Team);

                Assert.Equal(2, roster.Racers.Count());

            }
        }

        [Fact]
        public void AddSponsor_SimplyAdd()
        {
            var connectionStringBuilder =
        new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                Team t1 = new Team()
                {
                    Id = 1,
                    Name = "Stal Gorzów"
                };
                context.Teams.Add(t1);

                SpeedwayRepository repo = new SpeedwayRepository(context);
                repo.AddSponsor(new Sponsor()
                {
                    Id = 4,
                    Name = "Moje Bermudy",
                    Season = "2020",
                    Team = t1
                });

                Assert.True(repo.SaveChanges());
            }

        }

        [Fact]
        public void AddContest_SimplyAdd()
        {
            var connectionStringBuilder =
new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                SpeedwayRepository repo = new SpeedwayRepository(context);

                Contest contest = new Contest()
                {
                    Id = 2,
                    Name = "Speedway Grand Prix",
                    Country = "International",
                    Type = Entites.Enums.ContestType.Individual
                };

                repo.AddContest(contest);

                contest = new Contest()
                {
                    Id = 5,
                    Name = "Ekstraliga",
                    Country = "Poland",
                    Type = Entites.Enums.ContestType.Teams
                };

                repo.AddContest(contest);

                Assert.True(repo.SaveChanges());
            }
        }

        [Fact]
        public void AddContest_ShouldThrowException()
        {
            var connectionStringBuilder =
new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                SpeedwayRepository repo = new SpeedwayRepository(context);

                Contest contest = new Contest()
                {
                    Id = 2,
                    Name = "Speedway Grand Prix",
                    Type = Entites.Enums.ContestType.Individual
                };

                repo.AddContest(contest);

                Assert.ThrowsAny<Exception>(
                    () => repo.SaveChanges()
                    );
            }
        }
        [Fact]
        public void GetTeams_ForContest_ShouldRetriveAllTeams_ForGivenYears()
        {
            var connectionStringBuilder =
new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t1 = new Team()
                {
                    Name = "Stal Gorzów"
                };
                Team t2 = new Team()
                {
                    Name = "Sparta Wrocław"
                };
                Team t3 = new Team()
                {
                    Name = "Motor Lublin"
                };
                context.Teams.AddRange(new Team[] { t1, t2, t3 });
                Assert.Equal(3, context.SaveChanges());


                Contest contest = new Contest()
                {
                    Country = "Poland",
                    Name = "Ekstraliga",
                    Type = Entites.Enums.ContestType.Teams
                };

                context.Contests.Add(contest);
                Assert.Equal(1, context.SaveChanges());

                context.ContestTeams.AddRange(new ContestTeam[]
                    {
                        new ContestTeam()
                        {
                             Contest = contest,
                             Team = t1,
                             Season = "2019"
                        },
                        new ContestTeam()
                        {
                            Contest = contest,
                            Team = t2,
                            Season = "2019"
                        },
                        new ContestTeam()
                        {
                            Contest = contest,
                            Team = t3,
                            Season = "2020"
                        }
                    });
                Assert.Equal(3, context.SaveChanges());

                SpeedwayRepository repo = new SpeedwayRepository(context);

                FullContestTeam ct = repo.GetTeamsForContest(contest, "2020");

                Assert.Equal("2020", ct.Season);
                Assert.Single(ct.Teams);

                ct = repo.GetTeamsForContest(contest, "2019");
                Assert.Equal("2019", ct.Season);
                Assert.Equal(2, ct.Teams.Count());

                ct = repo.GetTeamsForContest(contest);

                Assert.Equal("2020", ct.Season);
                Assert.Single(ct.Teams);

            }
        }



    }
}
