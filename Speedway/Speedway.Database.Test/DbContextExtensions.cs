﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Speedway.Database.Contexts;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit.Abstractions;

namespace Speedway.Database.Test
{
    internal static class DbContextExtensions
    {
        public static DbContextOptionsBuilder<SpeedwayDbContext> UseDefaultLogger(this DbContextOptionsBuilder<SpeedwayDbContext> context, ITestOutputHelper _output)
        {
            return context.UseLoggerFactory(new LoggerFactory(
                        new[] { new LogToActionLoggerProvider((log) =>
                                {
                                   _output.WriteLine(log);
                                }) }));
        }
    }
}