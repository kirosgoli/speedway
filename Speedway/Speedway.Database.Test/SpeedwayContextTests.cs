using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Speedway.Database.Contexts;
using Speedway.Database.Entites;
using System;
using Xunit;
using Xunit.Abstractions;

namespace Speedway.Database.Test
{
    public class SpeedwayContextTests
    {
        private readonly ITestOutputHelper _output;

        public SpeedwayContextTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void AddRacer_SimpleAdd()
        {
            // Arrange

            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                context.Racers.Add(new Racer()
                {
                    Id = 2,
                    Name1 = "Bartosz",
                    Surname = "Zmarzlik",
                    Nationality1 = "Poland",
                    Birthday = new DateTime(1995, 04, 12)
                });

                context.Racers.Add(new Racer()
                {
                    Id = 3,
                    Name1 = "Artiom",
                    Name2 = "Grigorjewicz",
                    Surname = "�aguta",
                    Nationality1 = "Russia",
                    Nationality2 = "Poland",
                    Birthday = new DateTime(1995, 11, 13)
                });

                Assert.Equal(2, context.SaveChanges());
            }
        }

        [Fact]
        public void AddRacer_ShouldThrowException()
        {
            // Arrange

            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                context.Racers.Add(new Racer()
                {
                    Id = 2,
                    Name1 = "Test"
                });

                Assert.ThrowsAny<Exception>(
                    () => context.SaveChanges()
                    );
            }
        }

        [Fact]
        public void AddTeam_SimpleAdd()
        {
            // Arrange

            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                context.Teams.Add(new Team()
                {
                    Id = 4,
                    Name = "Unia Leszno",
                    CreationDate = DateTime.Now
                });

                Assert.Equal(1, context.SaveChanges());
            }
        }

        [Fact]
        public void AddTeam_ShouldThrowException()
        {
            // Arrange

            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                context.Teams.Add(new Team()
                {
                    Id = 4
                });

                Assert.ThrowsAny<Exception>(
                    () => context.SaveChanges()
                    );
            }
        }

        [Fact]
        public void AddSquat_SimpleAdd()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t = new Team()
                {
                    Id = 4,
                    Name = "Stal Gorz�w"
                };
                context.Teams.Add(t);

                Racer r = new Racer()
                {
                    Id = 2,
                    Name1 = "Bartosz",
                    Surname = "Zmarzlik",
                    Nationality1 = "Poland",
                    Birthday = new DateTime(1995, 04, 12)
                };
                context.Racers.Add(r);

                Assert.Equal(2, context.SaveChanges());

                Roster s = new Roster()
                {
                    Racer = r,
                    Team = t,
                    Season = "2020"
                };

                context.Rosters.Add(s);

                Assert.Equal(1, context.SaveChanges());
            }
        }

        [Fact]
        public void AddSquat_ShouldThrowException()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t = new Team()
                {
                    Id = 4,
                    Name = "Stal Gorz�w"
                };
                context.Teams.Add(t);

                Racer r = new Racer()
                {
                    Id = 2,
                    Name1 = "Bartosz",
                    Surname = "Zmarzlik",
                    Nationality1 = "Poland",
                    Birthday = new DateTime(1995, 04, 12)
                };
                context.Racers.Add(r);

                Assert.Equal(2, context.SaveChanges());

                Roster s = new Roster()
                {
                    Racer = r,
                    //Team = t,
                    Season = "2020"
                };

                context.Rosters.Add(s);

                Assert.ThrowsAny<Exception>(() => context.SaveChanges());
            }
        }

        [Fact]
        public void AddSponsors_ToTeam()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t = new Team()
                {
                    Id = 4,
                    Name = "Start Gniezno"
                };
                context.Teams.Add(t);

                context.SaveChanges();

                context.Sponsors.AddRange(new Sponsor[]{
                new Sponsor()
                {
                    Id =1,
                    Name = "Car Gwarant",
                    Season = "2020",
                    Team = t
                },
                new Sponsor()
                {
                    Id =2,
                    Name = "Kapi Meble",
                    Season = "2020",
                    Team = t
                },
                new Sponsor()
                {
                    Id =3,
                    Name = "Budex",
                    Season = "2020",
                    Team = t
                }
                });

                Assert.Equal(3, context.SaveChanges());

            }
        }

        [Fact]
        public void AddSponsors_ShouldThrowException()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t = new Team()
                {
                    Id = 4,
                    Name = "Stal Gorz�w"
                };
                context.Teams.Add(t);

                context.SaveChanges();

                context.Sponsors.Add(new Sponsor()
                {
                    Id = 1,
                    Name = "Moje Bermudy",
                    Season = "2020",
                });

                Assert.ThrowsAny<Exception>(() => context.SaveChanges());

            }

        }
        [Fact]
        public void AddContest_ShouldThrowException()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();

                context.Contests.Add(new Contest()
                {
                    Id = 1,
                });

                Assert.ThrowsAny<Exception>(() => context.SaveChanges());

            }
        }
        [Fact]
        public void AddTeam_ToContest_SimplyAdd()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t1 = new Team()
                {
                    Name = "Stal Gorz�w"
                };
                Team t2 = new Team()
                {
                    Name = "Sparta Wroc�aw"
                };
                Team t3 = new Team()
                {
                    Name = "Motor Lublin"
                };
                context.Teams.AddRange(new Team[] { t1, t2, t3 });
                Assert.Equal(3, context.SaveChanges());


                Contest contest = new Contest()
                {
                    Country = "Poland",
                    Name = "Ekstraliga",
                    Type = Entites.Enums.ContestType.Teams
                };

                context.Contests.Add(contest);
                Assert.Equal(1, context.SaveChanges());

                context.ContestTeams.AddRange(new ContestTeam[]
                    {
                        new ContestTeam()
                        {
                             Contest = contest,
                             Team = t1,
                             Season = "2019"
                        },
                        new ContestTeam()
                        {
                            Contest = contest,
                            Team = t2,
                            Season = "2019"
                        },
                        new ContestTeam()
                        {
                            Contest = contest,
                            Team = t3,
                            Season = "2019"
                        }
                    });
                Assert.Equal(3, context.SaveChanges());

            }

        }

        [Fact]
        public void AddTeam_ToContest_ShouldThrowException()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Team t1 = new Team()
                {
                    Name = "Stal Gorz�w"
                };
                Team t2 = new Team()
                {
                    Name = "Sparta Wroc�aw"
                };
                Team t3 = new Team()
                {
                    Name = "Motor Lublin"
                };
                context.Teams.AddRange(new Team[] { t1, t2, t3 });
                Assert.Equal(3, context.SaveChanges());


                Contest contest = new Contest()
                {
                    Country = "Poland",
                    Name = "Ekstraliga",
                    Type = Entites.Enums.ContestType.Teams
                };

                context.Contests.Add(contest);
                Assert.Equal(1, context.SaveChanges());
                context.ContestTeams.Add(
                        new ContestTeam()
                        {
                            Team = t1,
                            Season = "2019"
                        });

                Assert.ThrowsAny<Exception>(
                    () => context.SaveChanges()
                    );

                context.ContestTeams.Add(
                        new ContestTeam()
                        {
                            Contest = contest,
                            Season = "2019"
                        });

                Assert.ThrowsAny<Exception>(
                    () => context.SaveChanges()
                    );

                Assert.ThrowsAny<Exception>(
                   () => context.ContestTeams.Add(
                new ContestTeam()
                {
                    Contest = contest,
                    Team = t3
                }));
            }

        }

        [Fact]
        public void AddRacer_ToContest_SimplyAdd()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Racer r1 = new Racer()
                {
                    Surname = "Zmarzlik",
                    Nationality1 = "Poland",
                    Name1 = "Bartosz"
                };
                Racer r2 = new Racer()
                {
                    Name1 = "Patryk",
                    Surname = "Dudek",
                    Nationality1 = "Poland"
                };
                Racer r3 = new Racer()
                {
                    Name1 = "Leon",
                    Surname = "Madsen",
                    Nationality1 = "Denmark"
                };
                context.Racers.AddRange(new Racer[] { r1, r2, r3 });
                Assert.Equal(3, context.SaveChanges());


                Contest contest = new Contest()
                {
                    Country = "International",
                    Name = "Speedway Grand Prix",
                    Type = Entites.Enums.ContestType.Individual
                };

                context.Contests.Add(contest);
                Assert.Equal(1, context.SaveChanges());

                context.ContestRacers.AddRange(new ContestRacer[]
                    {
                        new ContestRacer()
                        {
                             Contest = contest,
                             Racer = r1,
                             Season = "2019"
                        },
                        new ContestRacer()
                        {
                            Contest = contest,
                            Racer = r2,
                            Season = "2019"
                        },
                        new ContestRacer()
                        {
                            Contest = contest,
                            Racer = r3,
                            Season = "2020"
                        }
                    });
                Assert.Equal(3, context.SaveChanges());

            }

        }

        [Fact]
        public void AddRacer_ToContest_ShouldThrowException()
        {
            var connectionStringBuilder =
                new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            var options = new DbContextOptionsBuilder<SpeedwayDbContext>()
                .UseDefaultLogger(_output)
                .UseSqlite(connection)
                .Options;

            using (var context = new SpeedwayDbContext(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                Racer r1 = new Racer()
                {
                    Surname = "Zmarzlik",
                    Nationality1 = "Poland",
                    Name1 = "Bartosz"
                };
                Racer r2 = new Racer()
                {
                    Name1 = "Patryk",
                    Surname = "Dudek",
                    Nationality1 = "Poland"
                };
                Racer r3 = new Racer()
                {
                    Name1 = "Leon",
                    Surname = "Madsen",
                    Nationality1 = "Denmark"
                };
                context.Racers.AddRange(new Racer[] { r1, r2, r3 });
                Assert.Equal(3, context.SaveChanges());


                Contest contest = new Contest()
                {
                    Country = "International",
                    Name = "Speedway Grand Prix",
                    Type = Entites.Enums.ContestType.Individual
                };

                context.Contests.Add(contest);
                Assert.Equal(1, context.SaveChanges());
                context.ContestRacers.Add(
                        new ContestRacer()
                        {
                            Racer = r1,
                            Season = "2019"
                        });

                Assert.ThrowsAny<Exception>(
                    () => context.SaveChanges()
                    );

                context.ContestRacers.Add(
                        new ContestRacer()
                        {
                            Contest = contest,
                            Season = "2019"
                        });

                Assert.ThrowsAny<Exception>(
                    () => context.SaveChanges()
                    );

                Assert.ThrowsAny<Exception>(
                   () => context.ContestRacers.Add(
                new ContestRacer()
                {
                    Contest = contest,
                    Racer = r3
                }));
            }

        }

    }
}