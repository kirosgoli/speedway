﻿using Microsoft.EntityFrameworkCore;
using Speedway.Database.Entites;
using System;
using System.Collections.Generic;
using System.Text;

namespace Speedway.Database.Contexts
{
    public class SpeedwayDbContext : DbContext
    {
        public DbSet<Racer> Racers { get; set; }

        public DbSet<Roster> Rosters { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Sponsor> Sponsors { get; set; }

        public DbSet<Contest> Contests { get; set; }
        public DbSet<ContestTeam> ContestTeams { get; set; }
        public DbSet<ContestRacer> ContestRacers { get; set; }
        public SpeedwayDbContext()
        { }

        public SpeedwayDbContext(DbContextOptions<SpeedwayDbContext> options)
         : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Roster>().HasKey(sq => new { sq.Season, sq.RacerId, sq.TeamId });
            modelBuilder.Entity<ContestTeam>().HasKey(ct => new { ct.ContestId, ct.TeamId, ct.Season });
            modelBuilder.Entity<ContestRacer>().HasKey(cr => new { cr.ContestId, cr.RacerId, cr.Season });
        }
    }
}