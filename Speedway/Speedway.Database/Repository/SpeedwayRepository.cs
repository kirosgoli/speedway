﻿using Speedway.Database.Contexts;
using Speedway.Database.Entites;
using Speedway.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Speedway.Database.Repository
{
    public class SpeedwayRepository : ISpeedwayRepository
    {
        private SpeedwayDbContext _dbContext;

        public SpeedwayRepository(SpeedwayDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }
        #region Racers
        public void AddRacer(Racer racer)
        {
            _dbContext.Add(racer);
        }

        public Racer GetRacer(int id)
        {
            return _dbContext.Racers.FirstOrDefault(racer => racer.Id == id);
        }

        public Racer GetRacerBySurname(string surname)
        {
            return _dbContext.Racers.FirstOrDefault(racer => racer.Surname.Equals(surname, StringComparison.OrdinalIgnoreCase));
        }
        #endregion Racers

        #region Sponsors
        public void AddSponsor(Sponsor sponsor)
        {
            _dbContext.Sponsors.Add(sponsor);
        }

        #endregion Sponsors

        #region Contests
        public void AddContest(Contest contest)
        {
            _dbContext.Contests.Add(contest);
        }

        public FullContestTeam GetTeamsForContest(Contest contest, string season = "")
        {

            if (contest == null)
            {
                return null;
            }

            if (string.IsNullOrEmpty(season))
            {
                season = DateTime.Now.Year.ToString();
            }

            FullContestTeam oResult = new FullContestTeam()
            {
                Contest = contest,
                Season = season,
            };
            List<Team> teams = new List<Team>();

            _dbContext.ContestTeams.Where(ct => ct.Season == season && ct.Contest == contest).ToList()
                .ForEach(ct => teams.Add(ct.Team)
                );
            oResult.Teams = teams;

            return oResult;


        }


        #endregion Contests

        #region Roster
        public FullRosterTeam GetRosterForTeam(Team _team, string season = "")
        {
            FullRosterTeam oResult = null;

            if (!_dbContext.Teams.Contains(_team))
                return oResult;

            if (string.IsNullOrEmpty(season))
                season = DateTime.Now.Year.ToString();

            oResult = new FullRosterTeam()
            {
                Season = season,
                Team = _team
            };

            List<Racer> racers = new List<Racer>();

            _dbContext.Rosters.Where(sq => sq.Team == _team && sq.Season == season)
                .ToList().ForEach(
                    sq => racers.Add(sq.Racer)
                ) ;
            oResult.Racers = racers;

            return oResult;

        }
        #endregion Roster

        public bool SaveChanges()
        {
            return _dbContext.SaveChanges() > 0;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_dbContext != null)
                    {
                        _dbContext.Dispose();
                        _dbContext = null;
                    }
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}