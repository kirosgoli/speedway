﻿using Speedway.Database.Entites;
using Speedway.Database.Models;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace Speedway.Database.Repository
{
    public interface ISpeedwayRepository : IDisposable
    {
        void AddRacer(Racer racer);

        Racer GetRacer(int id);

        Racer GetRacerBySurname(string surname);

        void AddSponsor(Sponsor sponor);

        void AddContest(Contest contest);

        FullRosterTeam GetRosterForTeam(Team team, string season = "");

        FullContestTeam GetTeamsForContest(Contest contest, string season = "");

        bool SaveChanges();
    }
}