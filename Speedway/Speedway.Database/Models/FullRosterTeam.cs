﻿using Speedway.Database.Entites;
using System;
using System.Collections.Generic;
using System.Text;

namespace Speedway.Database.Models
{
    public class FullRosterTeam
    {
        public string Season { get; set; }
        public Team Team { get; set; }
        public IEnumerable<Racer> Racers { get; set; }
    }
}
