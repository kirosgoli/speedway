﻿using Speedway.Database.Entites;
using System;
using System.Collections.Generic;
using System.Text;

namespace Speedway.Database.Models
{
    public class FullContestTeam
    {
        public Contest Contest { get; set; }
        public IEnumerable<Team> Teams { get; set; }
        public String Season { get; set; }
    }
}
