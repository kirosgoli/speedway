﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Speedway.Database.Entites
{
    public class Racer
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name1 { get; set; }

        public string Name2 { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Nationality1 { get; set; }

        public string Nationality2 { get; set; }

        [Required]
        public DateTime Birthday { get; set; }
    }
}