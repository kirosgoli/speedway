﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Speedway.Database.Entites
{
    public class Roster
    {
        [Required]
        public int TeamId { get; set; }

        public Team Team { get; set; }

        [Required]
        public int RacerId { get; set; }

        public Racer Racer { get; set; }

        [Required]
        public String Season { get; set; }
    }
}