﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Speedway.Database.Entites
{
    public class ContestRacer
    {
        [Required]
        public string Season { get; set; }
        public Contest Contest { get; set; }
        [Required]
        public int ContestId { get; set; }
        public Racer Racer { get; set; }
        [Required]
        public int RacerId { get; set; }
    }
}
