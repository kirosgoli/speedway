﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Speedway.Database.Entites
{
    public class ContestTeam
    {        
        [Required]
        public int TeamId { get; set; }
        public Team Team { get; set; }
        [Required]
        public int ContestId { get; set; }
        public Contest Contest { get; set; }
        [Required]
        public string Season { get; set; }

    }
}
