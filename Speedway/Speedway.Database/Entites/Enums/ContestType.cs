﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Speedway.Database.Entites.Enums
{
    public enum ContestType
    {
        Individual = 0,
        Teams = 1,
        Pairs = 2
    }
}
